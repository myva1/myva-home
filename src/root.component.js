import React from "react";
import { BrowserRouter, Route } from "react-router-dom";
import Dashboard from "./components/Dashboard";

export default function Root(props) {
  return (
    <div className="mt-16">
      <BrowserRouter>
        <Route path="/home" component={Dashboard} />
      </BrowserRouter>
    </div>
  );
}
